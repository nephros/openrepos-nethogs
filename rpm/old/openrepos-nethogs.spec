%define upstream_name nethogs

Summary: a small 'net top' tool
Name: openrepos-nethogs
Version: 0.8.6
Release: 1
License: GPL
#Group: System Environment/Base
URL: https://github.com/raboof/nethogs

#Source: https://github.com/raboof/nethogs/archive/v%%{version}.tar.gz
Source0: https://github.com/raboof/nethogs/archive/%{upstream_name}-%{version}.tar.gz
Source1: %{name}.desktop
Source2: %{name}_86.png
Source3: %{name}_100.png
Source4: %{name}_256.png

BuildRoot: %{_tmppath}/%{upstream_name}-%{version}-root

BuildRequires: libpcap-devel
BuildRequires: ncurses-devel
BuildRequires: gcc-c++
Requires: libpcap
Requires: ncurses-libs
Requires: libstdc++
Obsoletes: nethogs <= 0.8.1

%description
NetHogs is a small 'net top' tool. Instead of breaking the traffic down per
protocol or per subnet, like most tools do, it groups bandwidth by process.
NetHogs does not rely on a special kernel module to be loaded. If there's
suddenly a lot of network traffic, you can fire up NetHogs and immediately see
which PID is causing this. This makes it easy to indentify programs that have
gone wild and are suddenly taking up your bandwidth.

%prep
%autosetup -p1 -n %{upstream_name}-%{version}

%build
make %{?_smp_mflags} -s -l4.8 nethogs

%install
%{__rm} -rf %{buildroot}
%{__make} install PREFIX="%{buildroot}"/usr
%{__install} -p -D -m 644 %{SOURCE1} $RPM_BUILD_ROOT/%{_datadir}/applications/%{name}.desktop
%{__install} -p -D -m 644 %{SOURCE2} $RPM_BUILD_ROOT/%{_datadir}/icons/hicolor/86x86/apps/%{name}.png
%{__install} -p -D -m 644 %{SOURCE3} $RPM_BUILD_ROOT/%{_datadir}/icons/hicolor/108x108/apps/%{name}.png
%{__install} -p -D -m 644 %{SOURCE4} $RPM_BUILD_ROOT/%{_datadir}/icons/hicolor/256x256/apps/%{name}.png
# do not install man:
rm -r $RPM_BUILD_ROOT/%{_datadir}/man


%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%license COPYING
%{_sbindir}/nethogs
%defattr(-, root, root, 0644)
%{_datadir}/icons/hicolor/86x86/apps/%{name}.png
%{_datadir}/icons/hicolor/108x108/apps/%{name}.png
%{_datadir}/icons/hicolor/256x256/apps/%{name}.png
%{_datadir}/applications/%{name}.desktop


%post
if [ -x /usr/sbin/setcap ]; then
  /usr/sbin/setcap cap_net_admin,cap_net_raw+pe %{_sbindir}/nethogs || :
else
  chmod 1777  %{_sbindir}/nethogs || :
fi
/usr/bin/update-desktop-database -q || :


%postun
/usr/bin/update-desktop-database -q || :


%changelog
* Sat Mar 28 15:51:38 CET 2020 Nephros <sailfish@nephros.org> - 0.8.6
- Package latest from github for sailfishOS
